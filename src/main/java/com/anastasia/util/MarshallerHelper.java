package com.anastasia.util;

import com.anastasia.data.Book;
import com.anastasia.data.BookStore;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 22.10.2019.
 * @author Kodoma.
 */
public class MarshallerHelper {

    private static final String XML_FILE = "src/main/resources/data/data.xml";

    public static void main(String[] args) {
        Book emp1 = new Book(1L, "Гарри Поттер", "Дж. К. Роулинг");
        Book emp2 = new Book(1L, "Гарри Поттер", "Дж. К. Роулинг");
        Book emp3 = new Book(1L, "Гарри Поттер", "Дж. К. Роулинг");

        List<Book> list = new ArrayList<>();

        list.add(emp1);
        list.add(emp2);
        list.add(emp3);

        BookStore dept = new BookStore();
        dept.setBooks(list);

        try {
            // saveBook JAXB context and instantiate marshaller
            JAXBContext context = JAXBContext.newInstance(BookStore.class);

            // (1) Marshaller : Java Object to XML content.
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            // Write to System.out
            m.marshal(dept, System.out);


            // Write to File
            File outFile = new File(XML_FILE);
            m.marshal(dept, outFile);

            System.err.println("Write to file: " + outFile);

            // (2) Unmarshaller : Read XML content to Java Object.
/*            Unmarshaller um = context.createUnmarshaller();

            // XML file saveBook before.
            BookStore deptFromFile = (BookStore) um.unmarshal(new FileReader(XML_FILE));
            List<Book> emps = deptFromFile.getBooks();

            for (Book emp : emps) {
                System.out.println("Book name: " + emp.getName());
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
