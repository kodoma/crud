package com.anastasia.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 22.10.2019.
 * @author Kodoma.
 */
@XmlRootElement(name = "bookStore")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookStore {

    @XmlElementWrapper(name = "books")
    @XmlElement(name = "book")
    private List<Book> books;

    public BookStore() {
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void addBook(Book book) {
        if (this.books == null) {
            this.books = new ArrayList<>();
        }
        this.books.add(book);
    }
}
