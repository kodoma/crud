package com.anastasia.controller;

import com.anastasia.dao.BookDao;
import com.anastasia.dao.XMLBookDao;
import com.anastasia.data.Book;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class Controller {

    private BookDao bookDao;

    @FXML
    private Button createButton;

    @FXML
    private TableView<Book> bookTable;

    @FXML
    private TableColumn<Book, Long> bookTableIdColumn;

    @FXML
    private TableColumn<Book, String> bookTableNameColumn;

    @FXML
    private TableColumn<Book, String> bookTableAuthorColumn;

    private ObservableList<Book> bookData = FXCollections.observableArrayList();

    @FXML
    private void initialize() {
        // Инициализируем объект доступа к данным
        bookDao = new XMLBookDao();

        // устанавливаем тип и значение которое должно хранится в колонке
        bookTableIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        bookTableNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        bookTableAuthorColumn.setCellValueFactory(new PropertyValueFactory<>("author"));

        // заполняем таблицу данными
        bookTable.setItems(bookData);

        // подготавливаем данные для таблицы
        fillBookData();
    }

    public void createButtonAction(ActionEvent event) {
        final Book savedBook = bookDao.saveBook(new Book(1L, "Гарри Поттер", "Дж. К. Роулинг"));

        bookData.add(savedBook);
    }

    private void fillBookData() {
        bookData.addAll(bookDao.getAllBooks());
    }
}
