package com.anastasia.dao;

import com.anastasia.data.Book;

import java.util.List;

/**
 * Created on 22.10.2019.
 * @author Kodoma.
 */
public interface BookDao {

    Book saveBook(Book book);

    List<Book> getAllBooks();
}
