package com.anastasia.dao;

import com.anastasia.data.Book;
import com.anastasia.data.BookStore;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 22.10.2019.
 * @author Kodoma.
 */
public class XMLBookDao implements BookDao {

    private static final File OUT_FILE = new File("src/main/resources/data/data.xml");

    private static Marshaller marshaller = null;
    private static Unmarshaller unmarshaller = null;

    static {
        try {
            final JAXBContext context = JAXBContext.newInstance(BookStore.class);

            marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            unmarshaller = context.createUnmarshaller();
        } catch (JAXBException e) {
            System.err.print("Create marshaller error");
        }
    }

    @Override
    public Book saveBook(Book book) {
        final BookStore bookStore = getBookStore();

        bookStore.addBook(book);
        try {
            marshaller.marshal(bookStore, OUT_FILE);
        } catch (JAXBException e) {
            System.err.println("Create: error writing to file");
        }
        return book;
    }

    @Override
    public List<Book> getAllBooks() {
        final List<Book> books = getBookStore().getBooks();

        if (books == null) {
            return new ArrayList<>();
        }
        return books;
    }

    private BookStore getBookStore() {
        if (OUT_FILE.length() == 0) {
            return new BookStore();
        } else {
            try {
                return (BookStore) unmarshaller.unmarshal(OUT_FILE);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void main(String[] args) {
        new XMLBookDao().saveBook(new Book(1L, "Гарри Поттер", "Дж. К. Роулинг"));
    }
}
